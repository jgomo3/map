(ns jgomo3.playground.my-awesome-project.api
  (:gen-class)
  (:require [compojure.core :as c]
            [compojure.route :as route]
            [hiccup2.core :as h]
            #_[muuntaja.core :as m]
            [muuntaja.middleware :as mm]
            [ring.adapter.jetty :as jetty]
            [ring.middleware.defaults :as ring-defaults]))

(defonce server (atom nil))

(defn home-view [count]
  [:html
   [:body
    [:h1 "Welcome home!"]
    [:ul
     (for [i (range count)]
       [:li i])]]])

(defn routes []
  (c/routes
   (c/GET "/" [count]
          {:status 200
           :headers {"Content-Type" "text/html"}
           :body (str (h/html (home-view (Integer. (or count 0)))))})
   (c/GET "/:foo" [foo]
          {:status 200
           :body (str "you asked for " foo)})
   (c/POST "/api" [:as  req]
           (clojure.pprint/pprint req)
           {:status 200
            :body {:hello 123}})))

(defn handler [req]
  ((routes) req))

(defn start-jetty! []
  (reset!
   server 
   (jetty/run-jetty (-> #'handler
                        mm/wrap-format
                        (ring-defaults/wrap-defaults ring-defaults/api-defaults))
                    {:port 3428
                     :join? false})))

(defn stop-jetty! []
  (.stop @server)
  (reset! server nil))

(defn -main [& args]
  (start-jetty!))

(comment
 
(start-jetty!)
(stop-jetty!)
 
)
